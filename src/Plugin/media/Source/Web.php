<?php

namespace Drupal\web_media_source\Plugin\media\Source;

use Drupal\media\MediaInterface;
use Drupal\media\MediaTypeInterface;
use Drupal\media\MediaSourceBase;

/**
 * Web entity media source.
 *
 * @see \Drupal\link\Plugin\Field\FieldType\LinkItem 
 *
 * @MediaSource(
 *   id = "web",
 *   label = @Translation("Web"),
 *   description = @Translation("Use webs as reusable media."),
 *   allowed_field_types = {"link"},
 *   default_thumbnail_filename = "link.png"
 * )
 */
class Web extends MediaSourceBase {

  /**
   * {@inheritdoc}
   */
  public function getMetadataAttributes() {
    return [
     'title' => $this->t('Resource title'),
     'thumbnail_uri' => $this->t('Local URI of the thumbnail'),
     'default_name' => $this->t('Default name of the media item'),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getMetadata(MediaInterface $media, $attribute_name) {
    $media_url = $this->getSourceFieldValue($media);

    $resource = $media;
    try {
      // TODO try to get title (to implement if ever necessary....
      //$resource_url = $this->urlResolver->getResourceUrl($media_url);
      //$resource = $this->resourceFetcher->fetchResource($resource_url);
      
      // If the source field is not required, it may be empty.
      if (!$media_url) {
        return parent::getMetadata($media, $attribute_name);
      }
    }
    catch (ResourceException $e) {
      $this->messenger->addError($e->getMessage());
      return NULL;
    }
    
    switch ($attribute_name) {
    case 'default_name':
      if ($title = $this->getMetadata($media, 'title')) {
        return $title;
      }
      elseif ($media_url) {
        return $media_url;
      }
      return parent::getMetadata($media, 'default_name');
    case 'thumbnail_uri':
      // how can we obtain thumbnail via scrapping?
      //return $this->getLocalThumbnailUri($resource) ?: parent::getMetadata($media, 'thumbnail_uri');
      return parent::getMetadata($media, 'thumbnail_uri');
    case 'title':
      return $media->get($this->configuration['source_field'])->first()->{'title'};

    default:
      break;
    }
    return NULL;
  }

  /**
   * Gets the thumbnail image URI based on a media entity.
   *
   * @param \Drupal\media\MediaInterface $media
   *   A file entity.
   *
   * @return string
   *   File URI of the thumbnail image or NULL if there is no specific icon.
   */
  protected function getLocalThumbnailUri(MediaInterface $file) {
    /*
    // TODO Get remote thumbnail or generate it via web snapshot
    $remote_thumbnail_url = $resource->getThumbnailUrl();     
    if (!$remote_thumbnail_url) {       
      return NULL;     
    }
    copy remote to public:/;
    set thumbnail ad the obtained snapshot...
    return $snapshots_base . '/' . $snapshot_name . '.png';

     */

    return NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function createSourceField(MediaTypeInterface $type) {
    return parent::createSourceField($type)->set('settings', []);
  }

}
